if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper b0calc generate_b0 generate_b0calc generate_brain possum possumX possumX_postproc.sh possum_interpmot.py possum_matrix possum_plot.py possum_sum pulse signal2image spharm_rm systemnoise tcalc Possum_gui
fi
